﻿using Binance;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using Tulip;

namespace CryptoTrading.App.Algorthm.TradingStrategies
{
    public class RsiTradingStrategy : TradingStrategy
    {
        public RsiTradingStrategy(ILogger<TradingStrategy> logger) : base(logger)
        {
        }

        protected override double StrategyWeight => 1.0;


        //public override int OutputLength => 1000;

        protected override Dictionary<string, (Indicator indicator, double[] options)> GenerateIndicators()
        {
            var dict = new Dictionary<string, (Indicator indicator, double[] options)>();

            //add indicators to dictionary
            var rsi = (Tulip.Indicators.rsi, new double[] { 14 });
            //var ema = (Tulip.Indicators.ema, new double[] { 100 });
            dict.Add("Rsi", rsi);
            //dict.Add("LongEma", ema);
            return dict;
        }

        protected override double Calculate(Dictionary<string, double[][]> indicatorOutputs, Candlestick closePrice)
        {
            var rsi = indicatorOutputs["Rsi"][0].ToList();
            //var longEma = indicatorOutputs["LongEma"][0].ToList();

            Log($"RSI: {rsi.Last()}");
            Log($"RSI: {rsi.IndexOf(rsi.Count - 2)}");
            //Log($"Long Ema: {longEma.Last()}");
            Log($"Close Price: {closePrice}");

            // log values
            
            var condition1 = rsi.Last() > 20;
            var condition2 = rsi.IndexOf(rsi.Count-2) < 20; 
            //Price > than Long EMA
            //Long EMA is in an uptrend
            //Fast > Slow EMA
            if (condition1 && condition2)
            {
                LogResult(1);
                return 1;
            }
            //check if long is trading sideways, need more entries to determin that!

            //check if long is in an uptrend.
            LogResult(0);
            return 0;
        }

        
    }
}
