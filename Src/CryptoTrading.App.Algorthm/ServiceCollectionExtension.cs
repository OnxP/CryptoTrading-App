﻿using CryptoTrading.App.Algorthm.StopLimits;
using CryptoTrading.App.Algorthm.TradingStrategies;
using CryptoTrading.App.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Tulip;

namespace CryptoTrading.App.Algorthm
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAlgorthm(this IServiceCollection services)
        {
            services.AddTransient<ITradingStrategy, SimpleMacdTradingStrategy>();

            services.AddTransient<IAlgorthm, SimpleAlgorthm>();
            services.AddComposite<ITradingStrategy, CompositeTradingStrategy>();
            services.AddTransient<IStopLimitTracker, TrailingStopLimit>();

            return services;
        }

        public static IServiceCollection AddAlgorthm(this IServiceCollection services, double NoOfTrades, decimal Risk, decimal Increment)
        {
            services.AddTransient<ITradingStrategy, SimpleMacdTradingStrategy>(provider => 
            new SimpleMacdTradingStrategy(provider.GetService<ILogger<TradingStrategy>>(), NoOfTrades));

            services.AddTransient<IAlgorthm, SimpleAlgorthm>();
            services.AddComposite<ITradingStrategy, CompositeTradingStrategy>();
            services.AddTransient<IStopLimitTracker, TrailingStopLimit>(provider=> new TrailingStopLimit(Risk,Increment));

            return services;
        }


        public static void AddComposite<TInterface, TConcrete>(this IServiceCollection services)
  where TInterface : class
  where TConcrete : class, TInterface
        {
            var wrappedDescriptors = services.Where(s => s.ServiceType == typeof(TInterface)).ToList();
            foreach (var descriptor in wrappedDescriptors)
                services.Remove(descriptor);

            var objectFactory = ActivatorUtilities.CreateFactory(
              typeof(TConcrete),
              new[] { typeof(IEnumerable<TInterface>)});

            services.Add(ServiceDescriptor.Describe(
              typeof(TInterface),
              s => (TInterface)objectFactory(s, new[] { wrappedDescriptors.Select(d => s.CreateInstance(d)).Cast<TInterface>() }),
              wrappedDescriptors.Select(d => d.Lifetime).Max())
            );
        }

        public static object CreateInstance(this IServiceProvider services, ServiceDescriptor descriptor)
        {
            if (descriptor.ImplementationInstance != null)
                return descriptor.ImplementationInstance;

            if (descriptor.ImplementationFactory != null)
                return descriptor.ImplementationFactory(services);

            return ActivatorUtilities.GetServiceOrCreateInstance(services, descriptor.ImplementationType);
        }
    }
}
