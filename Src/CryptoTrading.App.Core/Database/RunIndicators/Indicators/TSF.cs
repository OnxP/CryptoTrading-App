﻿using CryptoTrading.App.Core.Database.Indicators;
using System;
using Tulip;

namespace CryptoTrading.App.Core.Database.RunIndicators.Indicators
{
    public class Tsf : IndicatorBaseDb
    {
        public double TsfValue { get; set; }
    }
    public class TsfIndicator : RunIndicatorBase<IndicatorContext<Tsf>, Tsf>
    {
        public override Indicator Indicator => Tulip.Indicators.tsf;

        public TsfIndicator(params decimal[] option) : base(option)
        {
        }

        protected override Tsf AddToDb(int candlestickId, params decimal[] outputs)
        {
            return new Tsf() { CandleStickId = candlestickId, TsfValue = Convert.ToDouble(outputs[0]) };
        }
        protected override void SaveContext()
        {
            context.Values.AddRange(list);
        }
    }
}
