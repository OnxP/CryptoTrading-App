﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoTrading.App.Core
{
    public enum RunTypeEnum
    {
        BackTesting,
        LiveTesting,
        Live
    }
}
