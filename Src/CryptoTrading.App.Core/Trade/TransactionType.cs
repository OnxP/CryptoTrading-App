﻿namespace CryptoTrading.App.Core.Trade
{
    public enum TransactionType{
        StopLimitTransaction,
        Transaction,
        MarketTransaction
    }
}