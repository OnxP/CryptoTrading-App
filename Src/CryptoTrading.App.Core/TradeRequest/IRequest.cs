﻿namespace CryptoTrading.App.Core.TradeRequest
{
    public interface IRequest
    {
        string Symbol { get; }
    }
}
